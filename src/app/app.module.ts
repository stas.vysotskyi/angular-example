import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { WebSocketsModule } from '@modules/web-sockets/web-sockets.module';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';

@NgModule({
  declarations: [ AppComponent],
  imports: [
    AppRoutingModule,
    BrowserModule,
    WebSocketsModule,
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule {}
