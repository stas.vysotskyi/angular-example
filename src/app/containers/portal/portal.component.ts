import { Component } from '@angular/core';

import { WebSocketsService } from '@modules/web-sockets/web-sockets.service';

@Component({
  templateUrl: './portal.component.html',
  styleUrls: ['./portal.component.scss']
})
export class PortalComponent {

  message = '';

  users = 0;
  messages: string[] = [];

  constructor(private webSocketsService: WebSocketsService) {}

  ngOnInit() {
    this.initUsersStream();
    this.initMessagesStream();
  }

  sendMessage(text: string) {
    this.messages.push(text);
    this.webSocketsService.sendMessage(text);
  }

  private initMessagesStream() {
    this.webSocketsService.receiveMessages()
    .subscribe(message => this.messages.push(message));
  }

  private initUsersStream() {
    this.webSocketsService.getUsers()
      .subscribe(users => this.users = users);
  }
}
