import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { PortalComponent } from './portal.component';
import { PortalRoutingModule } from './portal.routing';

@NgModule({
  declarations: [
    PortalComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    PortalRoutingModule
  ]
})
export class PortalModule {}
