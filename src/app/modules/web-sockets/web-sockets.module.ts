import { NgModule } from '@angular/core';

import { SocketIoModule } from 'ngx-socket-io';

import { WebSocketsService } from './web-sockets.service';

import { environment } from '@config/environment';

@NgModule({
  imports: [
    SocketIoModule.forRoot({ url: environment.socketHost }),
  ],
  providers: [WebSocketsService],
})
export class WebSocketsModule {}
