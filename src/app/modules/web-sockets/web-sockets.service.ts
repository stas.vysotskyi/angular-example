import { Injectable } from '@angular/core';

import { Socket } from 'ngx-socket-io';
import { Observable } from 'rxjs';

@Injectable()
export class WebSocketsService {

  constructor(private socket: Socket) {}

  sendMessage(text: string){
    this.socket.emit('stream', {
      text,
      date: new Date().toISOString()
    });
  }

  receiveMessages(): Observable<string> {
    return this.socket.fromEvent<string>('stream');
  }

  getUsers(): Observable<number> {
    return this.socket.fromEvent<number>('users');
  }
}
